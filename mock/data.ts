export const routes = [
  {
    id: 1,
    path: '/',
    name: 'System',
    component: 'Layout',
    hidden: false,
    children: [
      {
        path: 'system',
        component: 'system/systemView',
        name: '系统管理',
        hidden: false,
        meta: { title: '用户管理', icon: 'User' }
      }
    ]
  }
]

export const user = {
  id: 1,
  name: '张三',
  avatar: "https://foruda.gitee.com/avatar/1676958532593564854/1246463_coocie_1578947288.png"
}
