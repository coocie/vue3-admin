import type { MockMethod } from 'vite-plugin-mock'
import { routes, user } from './data'
export default [
  {
    url: '/api/login/login',
    method: 'post',
    response: (data: any) => {
      if (data.body.account == 'admin' && data.body.password == '123456') {
        return {
          code: 20000,
          data: {
            token: Math.random()
          },
        }
      } else {
        return {
          code: 20001,
          msg: '用户名或密码错误',
          data: {},
        }
      }
    },
  },
  // 获取动态路由参数
  {
    url: '/api/getRoutes',
    method: 'post',
    response: {
      code: 20000,
      data: routes,
    },
  },
  // 获取用户信息
  {
    url: '/api/user/getUserInfo',
    method: 'get',
    response: {
      code: 20000,
      data: user,
    },
  },

] as MockMethod[]
