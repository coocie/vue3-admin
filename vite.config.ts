import type { UserConfigExport, ConfigEnv } from 'vite'
import { fileURLToPath, URL } from 'node:url'
import { viteMockServe } from 'vite-plugin-mock'
import vue from '@vitejs/plugin-vue'

// https://vitejs.dev/config/
// https://www.npmjs.com/package/vite-plugin-mock?activeTab=readme
export default ({ command }: ConfigEnv): UserConfigExport => {
  return {
    plugins: [
      vue(),
      viteMockServe({
        // default
        mockPath: 'mock',
        // localEnabled: false
        localEnabled: command === 'serve'
      }),
    ],
    resolve: {
      alias: {
        '@': fileURLToPath(new URL('./src', import.meta.url))
      }
    },
    css: {
      preprocessorOptions: {
        scss: {
          additionalData: '@import "@/assets/scss/global.scss";'
        }
      }
    },
    server: {
      open: true,
      proxy: {
        '/api': {
          target: 'http://www.cry7.com',
          changeOrigin: true,
        }
      }
    },
    build: {
      // 如果自己分片的话，就会生成37个文件
      // chunkSizeWarningLimit: 1000,
      // rollupOptions: {
      //   output: {
      //     manualChunks(id) {
      //       if (id.includes('node_modules')) {
      //         return id.toString().split('node_modules/')[1].split('/')[0].toString();
      //       }
      //     }
      //   }
      // }
      // 不分片的话只有10个，但是有几个文件很大，打包时候会有提示，这里限制2M吧，如果再由超出提示就用上面那个
      chunkSizeWarningLimit: 2048,
    }
  }
}
