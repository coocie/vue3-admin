# vue3 管理端模板

vue3 element-plus vite axios pinia

### 启动
cnpm i
npm run dev

### 打包
npm run build

### 判断当前运行环境
console.log(import.meta.env.MODE)

### 状态
20000 正常
20001 登录失败

### 内部页面盒子
.mainbox

### 提示
import { ElMessage } from "element-plus";
ElMessage.success("操作成功");
ElMessage.error(res.data.msg);

### 按钮图标
```
<el-button type="primary" plain size="mini" @click="handleAdd"
  ><el-icon><Plus /></el-icon> <span>新增</span>
</el-button>
```