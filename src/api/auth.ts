import request from "@/utils/request"

export function login(data: object) {
  return request({
    url: '/login/login',
    method: 'post',
    data: data
  })
}

// 获取用户信息
export function getUserInfo() {
  return request({
    url: '/user/getUserInfo',
    method: 'get'
  })
}
