import request from "@/utils/request"

export function getData() {
  return request({
    url: '/dashboard/getData',
    method: 'get'
  })
}

