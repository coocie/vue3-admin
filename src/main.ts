import { createApp, createVNode } from 'vue'
import router from './router'
import pinia from '@/stores/store'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import App from './App.vue'
import * as Icons from '@element-plus/icons-vue'

import './assets/main.css'
import './permission' // permission control

const app = createApp(App)

app.use(router)
app.use(pinia)
app.use(ElementPlus)
app.mount('#app')

// 创建Icon组件
for (const [key, component] of Object.entries(Icons)) {
  app.component(key, component)
}
