import { createRouter, createWebHistory } from 'vue-router'
import Layout from '@/components/layout/layout.vue'

// isHide:是否显示

export const constantRoutes = [
  {
    path: '/',
    name: 'home',
    component: Layout,
    redirect: '/dashboard',
    children: [
      {
        path: 'dashboard',
        component: () => import('@/views/dashboard/dashboardView.vue'),
        name: '首页',
        meta: { title: 'Dashboard', icon: 'HomeFilled', affix: true }
      }
    ]
  },
  {
    path: '/system/',
    name: '系统',
    component: Layout,
    children: [
      {
        path: 'menu',
        component: () => import('@/views/system/menu/menuView.vue'),
        name: '菜单管理',
        meta: { title: '菜单管理', icon: 'DataBoard', affix: true }
      },
      {
        path: 'role',
        component: () => import('@/views/system/role/roleView.vue'),
        name: '角色管理',
        meta: { title: '角色管理', icon: 'DataBoard', affix: true }
      },
      {
        path: 'system',
        component: () => import('@/views/system/systemView.vue'),
        name: '用户管理',
        meta: { title: '用户管理', icon: 'DataBoard', affix: true }
      }
    ]
  },
  {
    path: '/about',
    name: 'about',
    // route level code-splitting
    // this generates a separate chunk (About.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import('@/views/AboutView.vue')
  },
  {
    path: '/login',
    name: 'login',
    isHide: true,
    component: () => import('@/views/login.vue')
  }
]

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: constantRoutes
})

export default router
