import { defineStore } from 'pinia'

export const useUserStore = defineStore('users', {
  state: () => {
    return {
      id: 0,
      name: '',
      avatar: '',
    }
  }
})
