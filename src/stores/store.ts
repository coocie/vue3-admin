// 在这里而不是在main.ts中创建的原因是，permission.ts中使用pinia会出现未注册的错误
import { createPinia } from 'pinia'
const pinia = createPinia()
export default pinia;
