import { defineStore } from 'pinia'

// console.log('constantRoutes',constantRoutes)

export const useRouterStore = defineStore('router', {
  state: () => {
    return { 'list': <any>[] }
  }
})
