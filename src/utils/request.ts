// 封装axios，请求中间件
import axios from 'axios'
import { tansParams } from "./func"
import { ElMessageBox } from 'element-plus'
import { getToken,removeToken } from './auth'
import router from "@/router";

axios.defaults.headers['Content-Type'] = 'application/json;charset=utf-8'

// 创建axios实例
const service = axios.create({
  // axios中请求配置有baseURL选项，表示请求URL公共部分
  baseURL: import.meta.env.VITE_APP_BASEAPI,
  // 超时
  timeout: 10000
})

// 请求拦截器
service.interceptors.request.use((config: any) => {
  if (getToken()) {
    config.headers['access-token'] = getToken()
  }
  // get请求映射params参数
  if (config.method === 'get' && config.params) {
    let url = config.url + '?' + tansParams(config.params);
    url = url.slice(0, -1);
    config.params = {};
    config.url = url;
  }
  console.log(config)
  return config
})

// 响应拦截器
service.interceptors.response.use(res => {
  // 未设置状态码则默认成功状态
  const code = res.data.code || 20000;
  // 二进制数据则直接返回
  if (res.request.responseType === 'blob' || res.request.responseType === 'arraybuffer') {
    return res.data
  }
  if (code == 20001) {
    ElMessageBox.confirm('登录状态已过期，您可以继续留在该页面，或者重新登录', '系统提示', {
      confirmButtonText: '重新登录',
      cancelButtonText: '取消',
      type: 'warning'
    }
    ).then(() => {
      removeToken()
      router.push({ path: "/login" });
    })
  }
  return res.data
})

export default service
