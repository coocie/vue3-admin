// 保存用户授权登录状态
import Cookies from 'js-cookie'

const TokenKey = 'Admin-Token'

// cookie 1天过期
const Expires = 1

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token: string) {
  return Cookies.set(TokenKey, token, { expires: Expires })
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}
